import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {Document} from '../document-model';
@Component({
  selector: 'app-docs-list',
  templateUrl: './docs-list.component.html',
  styleUrls: ['./docs-list.component.css']
})
export class DocsListComponent implements OnInit {
  
  @Input() documentList: Document[];

  constructor() { }

  ngOnInit(): void {

  }
  formatDate(date) {
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (month < 10) {
        month = '0' + month;
    }

    if (day < 10) {
        day = '0' + day;
    }

    return day + '-' + month + '-' + date.getFullYear();
}

}
