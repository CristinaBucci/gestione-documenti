import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {InputTextModule} from 'primeng/inputtext';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { DocsListComponent } from './docs-list/docs-list.component';
import { PaginationComponent } from './pagination/pagination.component';
import { FooterComponent } from './footer/footer.component';
import {ToolbarModule} from 'primeng/toolbar';
import {ButtonModule} from 'primeng/button';
import { FormsModule} from '@angular/forms';
import {PaginatorModule} from 'primeng/paginator';
import {TableModule} from 'primeng/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TooltipModule} from 'primeng/tooltip';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchBarComponent,
    DocsListComponent,
    PaginationComponent,
    FooterComponent,

  ],
  imports: [
    BrowserModule,
    ToolbarModule,
    ButtonModule,
    FormsModule,
    InputTextModule,
    PaginatorModule,
    TableModule,
    BrowserAnimationsModule,
    TooltipModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
