export class Document{

    constructor(
        public id: number,
        public name: string,
        public description: string,
        // public file:File,
        public date: Date,
        public tags: string[],
        // public category : string []
     ) {


    }
}
