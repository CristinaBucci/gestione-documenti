import { Injectable } from '@angular/core';
import {Document} from './document-model';
@Injectable({
  providedIn: 'root'
})
export class DocumentsService {

  private documentList: Document [] = [
    new Document (1, 'Determinazione 2021/1142', 'Approvazione graduatoria nidi comunali', new Date(2021, 5, 3), ['Atti Amministrativi', 'Pubblica istruzione', 'Asili nido', 'Determinazione', 'Graduatoria']),
    new Document (2, 'Delibera di giunta 2021/1132', 'Atto di citazione dinanzi all\' ufficio del giudice di pace per risarcimento danni sinistro del 02/03/2018.', new Date(2021, 4, 29), ['Atti Amministrativi', 'Delibera di Giunta', 'Citazione', 'Costituzione in giudizio']),
    new Document (3, 'Determinazione 2021/1123', 'Fornitura articoli di vestiario per il personale ausiliario del traffico - Impegno di spesa 2021', new Date (2021, 4, 29), ['Determinazione', 'Atti Amministrativi', 'Impegno di spesa', 'Bilancio', 'Personale']),
    new Document (4, 'Avviso Pubblico 2021/1070', 'Avviso di proroga dei termini di presentazione delle domande di partecipazione alla selazione di candidati per la stipulazione di un contratto di formazione e lavoro a tempo pieno per un posto di specialista in informatica e telematica', new Date (2021, 4, 26), ['Avviso Pubblico', 'Proroga', 'Selezione Pubblica', 'Bando', 'Personale']),
    // tslint:disable-next-line: max-line-length
    new Document (5, 'Verbale 2021/992', 'Commissione interna di sorteggio segnalazione certificata di conformità edilizia e agibilità', new Date (2021, 4, 19), ['Atti Amministrativi', 'Verbale', 'Commissione Interna']),
    // tslint:disable-next-line: max-line-length
    new Document (6, 'Atto Sindacale 2021/1075', 'Nomina dei rappresentanti del comitato tecnico per l\'assistenza', new Date (2021, 4, 27), ['Atti Amministrativi', 'Atto Sindacale', 'Nomine', 'Comitato tecnico']),
    // tslint:disable-next-line: max-line-length
    new Document (7, 'Ordinanza 2021/1821', 'Istituzione varia segnaletica su viale Virgilio', new Date (2021, 4, 2), ['Atti Amministrativi', 'Segnaletica', 'Ordinanza']),
    new Document (8, 'Delibera del Consiglio di Amministrazione 2016/2895', 'Istituzione: bilancio di previsione 2016 - Approvazione terza variazione', new Date (2016, 10, 6), ['Atti Ente Istituzione', 'Consiglio di Amministrazione', 'Bilancio', 'Delibera']),
    new Document (9, 'Determinazione 2020/1082', 'Acquisto servizio di Rendicontazione F24. Determina a contrarre e aggiudicazione. Impegno di spesa', new Date (2020, 6, 21), ['Determinazione', 'Atti Amministrativi', 'Impegno di spesa']),
    new Document (10, 'Atto Presidente del Consiglio Comunale 2020/1076', 'Modifica gruppi consiliari, presa d\'atto della decadenza dei conmponenti nominati in rappresentanza dei gruppi XXX e YYY nelle commissioni permanenti e nella commissione pari opportunità', new Date (2020, 5, 31), ['Atto Presidente Consiglio Comunale', 'Atti Amministrativi', 'Gruppi consiliari', 'Commissioni permanenti', 'Pari opportunità' ]),
    new Document (11, 'Avviso Pubblico 2019/1069', 'Avviso per selezione rapida per la formazione di una graduatoria per agenti di Polizia Municipale', new Date (2019, 3, 15), ['Avvisi', 'Polizia Municipale', 'Personale', 'Graduatoria']),
    ];
    constructor() { }

    getDocumentList(): Document[] {
    return this.documentList;
  }

    /*searchText(text: string){
    const results: Document[] = [];
    for (let _document of this.documentList) {
      if (_document.substr(text)){

      }
     }


    /*for (let i= 0; i<this.documentList.length; i++){
        if (this.documentList.substr(text)!= -1){
          this.results.push(this.documentList[i]);
        }

      }

  }*/
}
